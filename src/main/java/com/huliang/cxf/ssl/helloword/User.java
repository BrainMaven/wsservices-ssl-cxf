package com.huliang.cxf.ssl.helloword;


/**
 * 
 * @author huliang
 * @version $Id: User.java, v 0.1 2013年8月11日 下午6:13:57 huliang Exp $
 */
public class User {

    private String name;
    private int    age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
