package com.huliang.cxf.ssl.helloword;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * HelloWorld服务接口
 * 
 * @author huliang
 * @version $Id: HelloWordFacade.java, v 0.1 2013年8月11日 下午6:14:58 huliang Exp $
 */
@WebService
public interface HelloWordFacade {

    /**
     * 
     * @param text
     * @return
     */
    String sayHello(@WebParam(name = "text") String text);

    /**
     * 
     * @return
     */
    @WebResult(name = "User")
    List<User> getUsers();
}
