package com.huliang.cxf.ssl.helloword.impl;

import java.util.ArrayList;
import java.util.List;

import com.huliang.cxf.ssl.helloword.HelloWordFacade;
import com.huliang.cxf.ssl.helloword.User;

/**
 * HelloWorld服务实现
 * 
 * @author huliang
 * @version $Id: HelloWordFacadeImpl.java, v 0.1 2013年8月11日 下午6:17:20 huliang Exp $
 */
public class HelloWordFacadeImpl implements HelloWordFacade {

    /**
     * @see com.huliang.cxf.ssl.helloword.HelloWordFacade#sayHello(java.lang.String)
     */
    @Override
    public String sayHello(String text) {
        return "Hello " + text;
    }

    /**
     * @see com.huliang.cxf.ssl.helloword.HelloWordFacade#getUsers()
     */
    @Override
    public List<User> getUsers() {
        List<User> list = new ArrayList<User>(2);
        
        User user = new User();
        user.setName("huliang");
        user.setAge(28);
        list.add(user);
        
        user = new User();
        user.setName("dream");
        user.setAge(50);
        list.add(user);

        return list;
    }

}
